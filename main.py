import datetime
import json
import logging
import os
import sys
from http.server import HTTPServer, BaseHTTPRequestHandler
from typing import Optional

import jinja2


class MyLittleServer(BaseHTTPRequestHandler):
    encoding = "utf-8"
    env: Optional[jinja2.Environment] = None
    data_file = os.path.join(os.path.dirname(sys.argv[0]), "data", "messages.json")

    def _render_html(self) -> str:
        if self.env is None:
            self.env = jinja2.Environment(
                loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(sys.argv[0]), "templates/")),
                autoescape=jinja2.select_autoescape()
            )
        return self.env.get_template("index.htm").render()

    def _load_messages(self) -> list[dict[str, str]]:
        if not os.path.isfile(self.data_file):
            return []
        with open(self.data_file, mode="r") as fp:
            return json.load(fp)

    def _store_messages(self, messages: list[dict[str, str]]) -> None:
        with open(self.data_file, mode="w+") as fp:
            json.dump(messages, fp)

    def do_GET(self) -> None:
        if self.path == "/":
            self._get_chat_history()
        elif self.path == "/messages" or self.path.startswith("/messages?"):
            self._get_messages()
        else:
            body = bytes("""
            <!DOCTYPE html>
            <html lang="en">
                <head><title>404 Not Found</title></head>
                <body><h1>404 Not Found</h1>/body>
            </html>
            """, self.encoding)
            self.send_response(404)
            self.send_header("Content-Type", "text/html")
            self.send_header("Content-Length", str(len(body)))
            self.send_header("Content-Encoding", self.encoding)
            self.end_headers()
            self.wfile.write(body)

    def _get_messages(self) -> None:
        last_message_id = 0
        uri, raw_params = self.path.split("?", 2)
        params = {}
        for param in raw_params.split("&"):
            p = param.split("=")
            params[p[0]] = None
            if len(p) > 1:
                params[p[0]] = p[1]
        if "lastMessageId" in params:
            last_message_id = int(params["lastMessageId"])
        payload = self._load_messages()
        if last_message_id > 0:
            payload = payload[last_message_id:]
        body = bytes(json.dumps(payload), self.encoding)
        self.send_response(200)
        self.send_header("Content-Type", "application/json")
        self.send_header("Content-Length", str(len(body)))
        self.send_header("Content-Encoding", self.encoding)
        self.end_headers()
        self.wfile.write(body)

    def _get_chat_history(self) -> None:
        if self.env is None:
            self.env = jinja2.Environment(
                loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(sys.argv[0]), "templates/")),
                autoescape=jinja2.select_autoescape()
            )
        body = bytes(self._render_html(), self.encoding)
        self.send_response(200)
        self.send_header("Content-Type", "text/html")
        self.send_header("Content-Length", str(len(body)))
        self.send_header("Content-Encoding", self.encoding)
        self.end_headers()
        self.wfile.write(body)

    def do_POST(self) -> None:
        data_string = self.rfile.read(int(self.headers['Content-Length']))
        body = bytes("{}", self.encoding)
        status_code = 200
        error = False
        error_message = "400 Bad Request"
        if len(data_string):
            try:
                message = json.loads(data_string)
                if "username" not in message or not isinstance(message["username"], str) \
                        or "message" not in message or not isinstance(message["message"], str):
                    error = True
                    error_message = "Invalid message object!"
                else:
                    messages = self._load_messages()
                    messages.append({
                        "username": message["username"],
                        "message": message["message"],
                        "timestamp": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    })
                    self._store_messages(messages)
            except BaseException as e:
                error = True
                logging.error(e)
                error_message = str(e)
        if error:
            status_code = 400
            body = bytes(error_message, self.encoding)
        self.send_response(status_code)
        self.send_header("Content-Type", "application/json")
        self.send_header("Content-Length", str(len("2")))
        self.end_headers()
        self.wfile.write(body)


def run(server_class=HTTPServer, handler_class=MyLittleServer):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()


if __name__ == "__main__":
    run()
